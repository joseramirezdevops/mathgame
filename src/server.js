
//nodejs module required to start a webserver 
const express = require('express');
const app = express();


//To service the static frontend HTML page
app.use(express.static(`${__dirname}/frontend`));


//Defines listening port to show the webpage on localhost 
const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log(`To view your app, open this link in your browser: http://localhost: + ${port}`);

});



